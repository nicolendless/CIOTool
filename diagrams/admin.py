# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Diagram, Layer, TextField, Arrow

admin.site.register(Diagram)
admin.site.register(Layer)
admin.site.register(TextField)
admin.site.register(Arrow)
