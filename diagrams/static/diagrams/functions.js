
// Actions to do when adding layer through form avoinding refresh django
$(document).on('submit', '#add-layer', function(e) {
    e.preventDefault();

    $.ajax({
        type:'POST',
        url: 'add_layer/',
        data: {
          name:$('#layer-name').val(),
          color:$('#add-color').css('backgroundColor'),
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(){
          console.log("añadida");
        }
    });
});


//Variable that keeps track of the number of capas we add to the diagram
var num_capas = 0;

// Function to toggle between the desired colors of the layer
var i = 0;
$('#add-color').click(function() {
  var colors = ["#B39EB5", "#77DD77", "#FFD1DC", "#FF6961", "#779ECB"];
  if (i < 5) {
    $(this).css("background-color", colors[i]);
    i ++;
  } else {
    i = 0;
    $(this).css("background-color", "#FFB347");
  }
});

// Function to get the color of the capa, and to add the capa to the diagram
$('#btn-add-layer').click(function() {
    num_capas = num_capas + 1;

   var color = $('#add-color').css('backgroundColor');

   if (num_capas == 1) {
     $("#clon1").css("visibility", "visible");
   } else {
     var $clon = clone_div_matching("clon");
     change_cloned_ids($clon);
     $(".panel").append($($clon));
   }
   apply_color(color, "#clon" + num_capas)
   assign_name_capa('#layer-name','#txtlayer'+num_capas)
  });

function assign_name_capa(in_id, out_id) {
  var name = $(in_id).val();
  $(out_id).val(name);
  $(in_id).val("");
}

function clone_div_matching(match) {
  // get the last DIV which ID starts with ^= "clon"
  var $div = $('div[id^='+ match +']:last');
  // Read the Number from that DIV's ID (i.e: 3 from "clon3")
  // And increment that number by 1
  var num = parseInt($div.prop("id").match(/\d+/g), 10 ) +1;
  // Clone it and assign the new ID (i.e: from num 4 to ID "klon4")
  var $clon = $div.clone(true, true).prop('id', 'clon'+num );
  return $clon;
}

function apply_color(color, id) {
  $(id + " .capa").css("background-color", color);
  $(id + " .flechas i").css("color", color);
}

function change_cloned_ids($clone) {
  $clone.find('[id]').each(function() {
    var $th = $(this);
    var newID = $th.attr('id').replace(/.$/, function(str) { return parseInt(str) + 1; });
    $th.attr('id', newID);
  });
}

// Function to invert the arrows
$('.editable-arrow-ud').click(function() {
  if(!($( this ).hasClass("non_click"))) {
    $("#" + $(this).attr('id')).toggleClass('fa-arrow-down fa-arrow-up');
  }

});

$('.editable-arrow-lr').click(function() {
  if(!($( this ).hasClass("non_click"))) {
    $("#" + $(this).attr('id')).toggleClass('fa-arrow-right fa-arrow-left');
  }
});


$('.txt_content').dblclick(function() {
    window.location.assign($(this).attr('href'));
})
// Functions to add description and file to text area
// we first obtain the id of the dblclicked textare to know where to save info
var id_textarea = "";  // variable that stores the last dblcliked textarea
var id_layer = "";    // same with layer, to make it easier to post in DB

$('textarea').dblclick(function () {
  $('#txtname').text($(this).val());
  var complete_id = $(this).attr('id');
  arr_id = complete_id.split('-');
  id_textarea = arr_id[1];
  id_layer = arr_id[2];
});

$(document).on('submit', '#add-textarea', function(e) {
    e.preventDefault();

    $.ajax({
        type:'POST',
        url: 'add_textarea/',
        data: {
          layer: id_layer,
          textarea: id_textarea,
          name: $('#txtname').text(),
          description: $('#desc-box').val(),
          csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
        },
        success:function(){
          console.log("txt añadido");
        }
    });
});


// Functions to save the whole diagram and finish the process
function get_arrow_classes() {
  var arrows = [];
  $(".arrow").each(function(index) {
    arrows[index] = $(this).attr("class");
  });
  return arrows;
}

function get_txtfields_content() {
  var txt_fields = [];
  $(".txt_content").each(function(index) {
    txt_fields[index] = $(this).val();
  });
  return txt_fields;
}

function get_txt_layers() {
  var txt_layers = [];
  $(".layer-name").each(function(index) {
    txt_layers[index] = $(this).val();
  });
  console.log()
  return txt_layers;
}


$(document).on('submit', '#save-diagram', function(e) {
    if ($('#diagram-name').val() != '') {
      e.preventDefault();

      $.ajax({
          type:'POST',
          url: '/diagrams/creation/save_diagram/',
          data: {
            arrows: get_arrow_classes(),
            name: $('#diagram-name').val(),
            txt_fields: get_txtfields_content(),
            txt_layers: get_txt_layers(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
          },
          success:function(){
            alert('Se ha guardado el Diagrama!');
          }
      });
    } else {
      alert('Debe introducir un nombre!')
    }
});

$(document).on('submit', '#next-step', function(e) {
    if ($('#diagram-name').val() != '') {
      e.preventDefault();

      window.location.assign('creation_next/');
      $.ajax({
          type:'POST',
          url: 'save_diagram/',
          data: {
            arrows: get_arrow_classes(),
            name: $('#diagram-name').val(),
            txt_fields: get_txtfields_content(),
            txt_layers: get_txt_layers(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
          },
          success:function(){
            alert('Se ha guardado el Diagrama!');
          }
      });
    } else {
      alert('Debe introducir un nombre!')
    }
});




function show_description(name, description, url) {
  $('#txtfield-name').text('Título');
  $('#description-box').val("");
  $('#file-url').text("");

  if(name != '') {
    $('#txtfield-name').text(name);
  }
  $('#description-box').val(description);
  console.log('url: ' + url)
  $('#file-url').attr('href', '/media/' + url);
  $('#file-url').text(url);
}
