# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404, render, render_to_response, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from .models import Diagram, Layer, TextField, Arrow
from .models import TextFieldForm

def login_user(request):
    context = {'not_validate': False}
    logout(request)
    if request.POST:
        username = request.POST['email']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/diagrams/', {'user': user})
        else:
            context = {'not_validate': True}
    return render(request, 'diagrams/login.html', context)


@login_required(login_url='login/')
def index(request):
    latest_diagram_list = Diagram.objects.order_by('-pub_date')[:5]
    context = {'latest_diagram_list': latest_diagram_list}
    return render(request, 'diagrams/index.html', context)

def detail(request, diagram_id):
    diagram = get_object_or_404(Diagram, pk=diagram_id)
    layers = diagram.layer_set.all()
    last_layer = layers[len(layers)-1]
    return render(request, 'diagrams/detail.html', {'diagram': diagram, 'last_layer': last_layer})

def creation(request):
    diagram = Diagram(pub_date=timezone.now())
    diagram.save()
    return render(request, 'diagrams/creation.html')

def add_layer(request):
    # collect the last created diagram (most previous action) to attach layer
    diagram = Diagram.objects.latest('pub_date')
    # fetch data from the add-layer form and create new layer with it
    if request.method == "POST":
        name = request.POST['name']
        color = request.POST['color']

        layer = Layer.objects.create(
            diagram = diagram,
            layer_name = name,
            color = color
        )

        # create 5 arrows and 6 txtfields for the whole layer (rembember diagram structure)
        for i in range(5):
            TextField.objects.create(
                layer = layer
            )
            Arrow.objects.create(
                layer = layer
            )
        TextField.objects.create(layer = layer)
        return HttpResponse('')

def add_textarea(request):
    diagram = Diagram.objects.latest('pub_date')
    if request.method == 'POST':
        id_layer = int(request.POST['layer'])
        id_textarea = int(request.POST['textarea'])
        name = request.POST['name']
        description = request.POST['description']

        layer = Layer.objects.filter(diagram=diagram)[id_layer-1]
        txt = TextField.objects.filter(layer=layer)[id_textarea-1]

        txt.txt_name = name
        txt.description = description
        txt.save()
        return HttpResponse("")

def save_diagram(request):
    diagram = Diagram.objects.latest('pub_date')
    if request.method == 'POST':
        arrows = request.POST.getlist('arrows[]')
        name = request.POST['name']
        txt_content = request.POST.getlist('txt_fields[]')
        txt_layers = request.POST.getlist('txt_layers[]')

        l = Layer.objects.filter(diagram=diagram)
        print l;
        for i in range(l.count()):
            layer_var = l[i]
            layer_var.layer_name = txt_layers[i]
            layer_var.save()

        txt = TextField.objects.filter(layer__diagram=diagram)
        print txt
        for i in range(txt.count()):
            txt_var = txt[i]
            txt_var.txt_name = txt_content[i]
            txt_var.save()

        a = Arrow.objects.filter(layer__diagram=diagram)
        for i in range(a.count()):
            arrow_var = a[i]
            arrow_var.direction_class = arrows[i]
            arrow_var.save()

        diagram.diagram_name = name
        diagram.save()
        return HttpResponse("")


def add_user(request):
    users = User.objects.all()
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        rol = request.POST['rol']

        user = User.objects.create_user(username, "", password)
        group = Group.objects.get(name=rol)
        group.user_set.add(user)
        user.save()
        group.save()
    return render(request, 'diagrams/create_user.html', {'users': users })

def modify_txt(request, txt_id):
    txt = get_object_or_404(TextField, pk=txt_id)
    print(txt.layer_id)
    if request.method == 'POST':
        form = TextFieldForm(request.POST, request.FILES)
        if form.is_valid():
            p = form.save(commit=False)
            txt.txt_name = p.txt_name
            txt.description = p.description
            txt.upload = p.upload
            txt.save()
            print txt.txt_name
            return redirect('/diagrams/creation/creation_next/')
    else:
        form = TextFieldForm({'layer': txt.layer, 'txt_name': txt.txt_name, 'description': txt.description, 'upload': txt.upload})        
    return render(request, 'diagrams/modify_txt.html', {'form': form, 'txt': txt})

def creation_next(request):
    diagram = Diagram.objects.latest('pub_date')
    layers = diagram.layer_set.all()

    last_layer = layers[len(layers)-1]
    return render(request, 'diagrams/creation_next.html', {'diagram': diagram, 'last_layer': last_layer})
