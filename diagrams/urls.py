from django.conf.urls import url

from . import views


app_name = 'diagrams' # optional in case you have more than one app
urlpatterns = [
    # ex: /diagrams/
    url(r'^$', views.index, name='index'),
    # the 'name' value as called by the {% url %} template tag
    url(r'^login/$', views.login_user, name='login'),
    url(r'^(?P<diagram_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^creation/$', views.creation, name='creation'),
    url(r'^creation/add_layer/$', views.add_layer),
    url(r'^creation/add_textarea/$', views.add_textarea),
    url(r'^creation/creation_next/$', views.creation_next),
    url(r'^creation/save_diagram/$', views.save_diagram),
    url(r'^add_user/$', views.add_user, name='add_user'),
    url(r'^(?P<txt_id>[0-9]+)/modify/$', views.modify_txt, name='modify_txt'),
    # url(r'^creation/modify_txt/$', views.modify_txt, name='modify_txt'),
]
