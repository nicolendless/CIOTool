# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.forms import ModelForm


class Diagram(models.Model):
    diagram_name = models.CharField(max_length=50)
    pub_date = models.DateTimeField('date published')


class Layer(models.Model):
    diagram = models.ForeignKey(Diagram, on_delete=models.CASCADE)
    layer_name = models.CharField(max_length=100)
    color = models.CharField(max_length=20)


class TextField(models.Model):
    layer = models.ForeignKey(Layer, on_delete=models.CASCADE)
    txt_name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    # file will be uploaded to MEDIA_ROOT/uploads
    upload = models.FileField(upload_to='uploads/')


class Arrow(models.Model):
    layer = models.ForeignKey(Layer, on_delete=models.CASCADE)
    direction_class = models.CharField(max_length=100)


class DiagramForm(ModelForm):
    class Meta:
        model = Diagram
        fields = ['diagram_name']


class LayerForm(ModelForm):
    class Meta:
        model = Layer
        fields = ['layer_name', 'color']


class TextFieldForm(ModelForm):
    class Meta:
        model = TextField
        fields = ['layer', 'txt_name', 'description', 'upload']
